import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import 'package:dart_sandbox/components/home/home_component.dart';
import 'package:dart_sandbox/components/profile/profile_component.dart';
import 'package:dart_sandbox/components/login/login_component.dart';
import 'package:dart_sandbox/components/secure_router_outlet.dart' show SecureRouterOutlet;
import 'package:dart_sandbox/services/security_service.dart';

@Component(
  selector: 'my-app',
  providers: const [SecurityService]
)
@View(templateUrl: 'app_component.html',
    directives: const [SecureRouterOutlet, RouterLink])
@RouteConfig(const [
  const Route(path: '/login', name: 'Login', component: LoginComponent, useAsDefault: true),
  const Route(path: '/home', name: 'Home', component: HomeComponent),
  const Route(path: '/profile', name: 'Profile', component: ProfileComponent)
])
/// [AppComponent] is the primary component for the app.
class AppComponent {

  //StreamSubscription<bool> _isLoggedInSub;
  SecurityService _security;

  ///
  bool isLoggedIn;

  /// [AppComponent] constructor.
  AppComponent(this._security) {
    _security.isLoggedInStream.listen((value) {
      print('value: $value');
      isLoggedIn = value;
    });
  }

  /// [logout()] logs out the user by calling _security.logout().
  void logout() {
    _security.logout();
  }
}
