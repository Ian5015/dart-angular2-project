import 'dart:async';

import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import 'package:dart_sandbox/services/security_service.dart';

@Component(selector: 'login', providers: const [SecurityService])
@View(templateUrl: 'login_component.html')

/// [LoginComponent] handles login.
class LoginComponent {

  final SecurityService _security;
  Router _router;

  /// [username] is bound to username input.
  @Input()
  String username;

  /// [password] is bound to password input.
  @Input()
  String password;

  /// Primary constructor.
  LoginComponent(this._security, this._router);

  /// Log a user in.
  Future login() async {
    final success = await _security.login(username, password);

    if (success) {
      await _router.navigate(['Home', {}]);
    }
  }
}
