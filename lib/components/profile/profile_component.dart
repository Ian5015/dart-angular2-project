import 'package:angular2/angular2.dart';

@Component(selector: 'profile')
@View(template: '<div>Profile Component</div>')
/// [ProfileComponent] displays the logged in user's profile and allows editing.
class ProfileComponent {}
