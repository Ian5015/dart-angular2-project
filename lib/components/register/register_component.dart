import 'dart:async';

import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import '../../models/user.dart';

import 'package:dart_sandbox/services/security_service.dart';

@Component(selector: 'register', providers: const [SecurityService])
@View(templateUrl: 'register_component.html')

/// [RegisterComponent] handles login.
class RegisterComponent {

  final SecurityService _security;
  Router _router;

  /// [username] is bound to username input.
  String username;

  /// [password] is bound to password input.
  String password;

  /// Primary constructor.
  RegisterComponent(this._security, this._router);

  /// [register] a new user.
  Future register() async {
    final success = await _security.register(new UserInputDTO());

    if (success) {
      await _router.navigate(['Home', {}]);
    }
  }
}
