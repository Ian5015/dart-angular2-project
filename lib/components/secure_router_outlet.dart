import 'dart:async';
import 'dart:html';

import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import 'package:dart_sandbox/components/login/login_component.dart' show LoginComponent;

@Directive(selector: 'router-outlet')
/// [SecureRouterOutlet] extends the default Angular2 [RouterOutlet].
/// This is used so that we can control which routes are public and restrict access
/// to private routes.
class SecureRouterOutlet extends RouterOutlet {

  /// [publicRoutes] contains all the public routes for the app.
  dynamic publicRoutes = {};
  final Router _parentRouter;

  /// Primary constructor for [SecureRouterOutlet].
  SecureRouterOutlet(ViewContainerRef _viewContainerRef, ComponentResolver componentResolver,
      Router parentRouter, @Attribute('name')String nameAttr) :
        _parentRouter = parentRouter,
        super(_viewContainerRef, componentResolver, parentRouter, nameAttr) {

    this.publicRoutes = {
      '/login': true,
      '/signup': true
    };
  }

  @override
  Future activate(ComponentInstruction instruction) {
    final url = this._parentRouter.lastNavigationAttempt;
    if(!this.publicRoutes[url] && !window.localStorage.containsKey('app-token')) {
      instruction.componentType = LoginComponent;
    }
    return super.activate(instruction);
  }
}