class User {
  /// User's [firstName].
  String firstName;
  /// User's [lastName].
  String lastName;
  /// User's [email].
  String email;
  /// User's [username].
  String username;
}

/// Used when created or updating a user.
class UserInputDTO extends User {
  /// User's [password].
  String password;
}