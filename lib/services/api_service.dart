import 'dart:async';
import 'dart:html';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:angular2/angular2.dart';

import '../services/app_settings_service.dart';

/// Service to communicate with an API.
@Injectable() class ApiService<T> {

  /// This is the api url used to make HTTP requests.
  String apiUrl = AppSettings.apiUrl;
  /// This is the api version used to make HTTP requests.
  String apiVersion = AppSettings.apiVersion;

  String _delete = 'delete';
  String _get = 'get';
  String _post = 'post';
  String _put = 'put';

  //'localhost:8080/api/userApi/0.1/get/5536ee1c578fbe0f68000001'
  /// Helper for making get requests to the API.
  Future<Payload<T>> httpGet(String controller, String action, {String id, dynamic filter}) async {
    final String getResponse = await HttpRequest
        .getString(urlBuilder(controller, action, id: id, data: filter));
    final Payload<T> decodedObject = JSON.decode(getResponse) as Payload<T>;


    return new Future<Payload<T>>.value(decodedObject);
  }

  Future<Payload<T>> httpGet2(String controller, String action, {String id, dynamic filter}) async {
    var getResponse = await http.get(urlBuilder(controller, action, id: id, data: filter));
    Payload<T> payload = JSON.decode(getResponse.body) as Payload<T>;
    return new Future<Payload<T>>.value(payload);
  }
  ///
  Future<Payload<T>> httpPost(String controller, String action, {String id, dynamic data}) async {
    final HttpRequest postResponse = await HttpRequest
        .postFormData('$apiUrl/$controller/$apiVersion/$action', new Map.from(data));
    final Payload<T> decodedObject = JSON.decode(postResponse.responseText) as Payload<T>;
    return new Future<Payload<T>>.value(decodedObject);
  }

  /// TODO put
  Future test(String controller, String action, {String id, dynamic filter}) async {
    var putResponse = await http.put('$apiUrl/$controller/$apiVersion/$action',);
  }

  /// TODO
  String urlBuilder(String controller, String action, {String id, dynamic data}) {
    String url = '$apiUrl/$controller/$apiVersion/$action';
    if(id != null && id.isNotEmpty) {
      url = url + id;
    } else if(data != null)  {
      url = url + new Uri(query: JSON.encode(data)).toString();
    } else {
      url = url;
    }
    return url;
  }
}

/// Defines a payload.
class Payload<T> {
  /// The [data] returned from the API in response to a request.
  T data;
  /// Collection of [errors], if any, returned by the API in response to a request.
  List<Error> errors;
  /// Whether the request was deemed a [success].
  bool success;
}