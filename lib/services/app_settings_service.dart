

abstract class AppSettings {
  /// The [apiUrl] provided by the environment.
  static String apiUrl = new String
      .fromEnvironment('apiUrl', defaultValue: 'http://localhost:8080/api/');
  /// The [apiVersion] provided by the environment.
  static String apiVersion = new String
      .fromEnvironment('apiVersion', defaultValue: '0.1');
}