import 'dart:async';
import 'dart:html';

import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import '../models/user.dart';

/// [SecurityService] handles authentication and permissions related actions.
@Injectable() class SecurityService {

  Router _router;

  /// Constructor for [SecurityService].
  SecurityService(this._router);

  // This token will be added to requests and validated on the server.
  String _token;

  // Controller to handle the stream so that the logged in state of the user
  // can be "broadcast" to listeners.
  static final StreamController<bool> _isLoggedInStreamController = new StreamController<bool>();

  /// [isLoggedInStream] provides a means for cross class communication via Stream.
  Stream<bool> get isLoggedInStream => _isLoggedInStreamController.stream;

  // Keep track of user's logged in status.
  static bool _isLoggedIn = false;

  /// [isLoggedIn] returns true if the user is logged in.
  bool get isLoggedIn => _isLoggedIn;

  /// [token] returns the auth token.
  String get token => _token;

  /// [login] logs in a user by calling the api.
  Future<bool> login(String username, String password) async{
    // Here we would authenticate with the server.
    // For now let's just simulate a login.
    _isLoggedIn = true;
    // This is fake data until I wire up the call to auth server.
    _token = '123';
    // Add the token to localStorage.
    window.localStorage.putIfAbsent('app-token', () => 'app_token');
    // Notify listeners about change in _isLoggedIn.
    _isLoggedInStreamController.add(_isLoggedIn);

    // TODO
    // This needs to represent whether the login attempt is a success.
    // For now I'm passing in true while testing the UI.
    return new Future.value(true);
  }

  /// [logout] will log a user out and clear the app-token from localStorage.
  void logout() {
    // Remove the token from localStorage.
    window.localStorage.remove('app-token');
    // Clear the local _token.
    _token = null;
    // Set local _isLoggedIn to false.
    _isLoggedIn = false;
    // Notify listeners about change in _isLoggedIn.
    _isLoggedInStreamController.add(_isLoggedIn);
    // Navigate to the login screen.
    _router.navigate(['Login', {}]);
  }

  /// [onDestroy] called by Angular when the service is destroyed.
  /// Contains cleanup of the service.
  void onDestroy() {
    // Clean the sink.
    _isLoggedInStreamController.close();
  }

  Future<bool> register(UserInputDTO input) async{
    return new Future.value(true);
  }
}