import 'dart:async';
import 'package:angular2/router.dart';

import 'package:angular2/angular2.dart';
import 'package:dart_sandbox/components/app_component.dart';

// needed to import "bootstrap" method
import 'package:angular2/platform/browser.dart';
// needed to import LocationStrategy and HashLocationStrategy
import 'package:angular2/platform/common.dart';

/// [main] is the main entry point for the app.
Future main() async {
  await bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    const Provider(LocationStrategy, useClass: HashLocationStrategy)
  ]);
}
