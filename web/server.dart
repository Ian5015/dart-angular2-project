import 'dart:io';
import 'dart:async';
import 'package:http_server/http_server.dart';
import 'package:path/path.dart';

Future main() async {
  final pathToBuild = join(dirname(Platform.script.toFilePath()));
  final staticFiles = new VirtualDirectory(pathToBuild);
  staticFiles.allowDirectoryListing = true;
  staticFiles.directoryHandler = (dir, request) {
    print(dir.path);
    final indexUri = new Uri.file(dir.path).resolve('index.html');
    staticFiles.serveFile(new File(indexUri.toFilePath()), request);
  };

  final server = await HttpServer.bind(InternetAddress.LOOPBACK_IP_V4, 4048);
  print('Listening on port 4048');
  await server.forEach(staticFiles.serveRequest);
}